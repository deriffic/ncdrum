(function ($, Drupal) {


  if( $('body').hasClass('path-frontpage')){ 

    	//------VIDEO BACKGROUND-----//

	  $('.video-background').attr('loop','loop');

	  $('#headerVideo').prepend('<div class="video-background"></div>');
	  
	  if( $('body').hasClass('path-frontpage') || $('body').hasClass('path-user') ){
	  	//alert('Pooo');
	  	 $('.header-wrapper').wrapInner('<div class="transparent"></div>');
	  }


	  $('.video-background').videobackground({
	    videoSource: [
	      ['/themes/ncdrum/video/drummer.mp4', 'video/mp4'],
	      ['/themes/ncdrum/video/drummer.webm', 'video/webm'], 
	      ['/themes/ncdrum/video/drummer.ogv', 'video/ogg']], 
	    controlPosition: '#headerVideo',
	    loop:true,
	    muted:true,
	    poster: '/themes/ncdrum/images/drummer.jpg',
	    resize:true,
	    
	    loadedCallback: function() {
	      $(this).videobackground('mute');
	    }
	  });

}

 			function headerCenter(){
	 		var windowHeight     = $(window).height();
	 		var windowHalf		 = windowHeight / 2;
	 		var promoHeight      = $('.section1-content').outerHeight();		
	 		var contentHeight 	 = promoHeight;
	 		var contentHalf    	 = contentHeight / 2;

	 		var navHeight 		 = $('.top-bar').outerHeight();

	 		var centerContent	 = windowHalf - contentHalf ;

	 	
	 		$('.section1-content').css('margin-top', centerContent);
	 	}

	    headerCenter();

		$(window).resize(function(event) {
		 	headerCenter();
		});


})(jQuery, Drupal);
