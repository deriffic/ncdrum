(function ($, Drupal) {

	    $(window).on('load',function() {
    		$('#status').delay(600).fadeOut('slow'); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
			$('body').delay(350).css({'overflow':'visible'});
    	});
    	
})(jQuery, Drupal);